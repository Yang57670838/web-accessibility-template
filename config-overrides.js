const path = require("path");

module.exports = {
  webpack: function (config, env) {
    // add plugin
    if (!config.plugins) {
      config.plugins = [];
    }
    config.resolve = {
      ...config.resolve,
      alias: {
        ...config.resolve.alias,
        assets: path.resolve(__dirname, "src/assets"),
        common: path.resolve(__dirname, "src/common"),
        screens: path.resolve(__dirname, "src/screens"),
      },
    };
    return config;
  },
  jest: function (config) {
    // ...add your jest config customisation...
    return config;
  },
  devServer: function (configFunction) {
    return function (proxy, allowedHost) {
      const config = configFunction(proxy, allowedHost);

      config.proxy = {
        ...config.proxy,
        "/bff": {
          // /api requrest send to local nodeJS app for test purpose, at port 4000
          target: "http://localhost:4000",
          secure: false,
          logLevel: "debug",
          // changeOrigin: true,
          pathRewrite: {
            "^/bff": "",
          },
        },
      };
      return config;
    };
  },
  paths: function (paths, env) {
    return paths;
  },
};
