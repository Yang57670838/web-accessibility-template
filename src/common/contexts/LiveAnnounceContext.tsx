import React, { createContext, ReactNode, useMemo } from "react";
import { LiveAnnouncer, LiveMessenger } from "react-aria-live";

export interface LiveAnnounceContextInterface {
  announcePolite: (m: string, id?: string) => void;
  announceAssertive: (m: string, id?: string) => void;
}

interface OwnProps {
  children: ReactNode;
}

const Context = createContext<LiveAnnounceContextInterface | null>(null);

export const LiveAnnounceStore: React.FC<OwnProps> = ({
  children,
}: OwnProps) => {
  return (
    <LiveAnnouncer>
      <LiveMessenger>
        {({ announcePolite, announceAssertive }) => (
          // eslint-disable-next-line react/jsx-no-constructed-context-values
          <Context.Provider value={{ announcePolite, announceAssertive }}>
            {children}
          </Context.Provider>
        )}
      </LiveMessenger>
    </LiveAnnouncer>
  );
};

export default Context;
