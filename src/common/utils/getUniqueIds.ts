import { v4 as uuidv4 } from "uuid";

export const getIdV4 = (): string => {
  return uuidv4();
};

export const getTraceId = (): string => {
  const result = uuidv4().split("-").join("");
  return result;
};
