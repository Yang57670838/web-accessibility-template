import { FC, useEffect, useRef } from "react";

import styles from "./index.module.css";

const Spinner: FC = () => {
  const pageSpinner = useRef<any>(null);
  useEffect(() => {
    if (pageSpinner.current) {
      pageSpinner.current.focus();
    }
  }, []);
  const focusOutHandler = () => {
    pageSpinner.current.focus();
  };
  return (
    <div
      tabIndex={0} // eslint-disable-line jsx-a11y/no-noninteractive-tabindex
      ref={pageSpinner}
      className={`${styles.loading}`}
      id="pageSpinner"
      onBlur={focusOutHandler}
    />
  );
};

export default Spinner;
