import { screen, render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import PageHeading from "./index";

describe("<PageHeading />", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test("test page header render correctly with focus", () => {
    render(<PageHeading heading1="heading" />);

    // header have no focus
    const header = screen.getByText("heading");
    expect(header).toBeInTheDocument();
    expect(header).toHaveFocus();
  });
});
