import { FC, useRef, useEffect } from "react";

export type OwnProps = {
  heading1: string;
};

const PageHeading: FC<OwnProps> = ({ heading1 }) => {
  const h1Ref = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    if (h1Ref.current) {
      h1Ref.current.focus();
    }
  }, []);
  return (
    <div>
      <h1 ref={h1Ref} tabIndex={-1}>
        {heading1}
      </h1>
    </div>
  );
};

export default PageHeading;
