import { FC, useEffect } from "react";
import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const CustomSelect: FC = () => {
  return (
    <div data-testid="custom-select">
      <Select options={options} />
    </div>
  );
};

export default CustomSelect;
