import { screen, render, fireEvent } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import CustomSelect from './index';
import {BrowserRouter, MemoryRouter} from 'react-router-dom'
import { isExportDeclaration } from 'typescript';

describe('<CustomSelect />', () => {
    afterEach(() => {
        jest.resetAllMocks();
    })

    test('test dropdown without keyboard accessibility', async () => {
        render(<CustomSelect />)
        
        const dropdownWrapper = screen.getByTestId("custom-select");
        expect(dropdownWrapper).toBeInTheDocument();
        fireEvent.keyDown(dropdownWrapper.firstChild as any, { key: 'ArrowDown' });
        fireEvent.click(screen.getByText('Vanilla'));
        expect(screen.getByText("Vanilla")).toBeInTheDocument();
        expect(screen.queryByText("chocolate")).not.toBeInTheDocument();
    });

    test('test dropdown with keyboard accessibility', () => {
        render(<CustomSelect />)
        
        userEvent.tab();

        const dropdownWrapper = screen.getByTestId("custom-select");
        expect(dropdownWrapper).toBeInTheDocument();
        expect(dropdownWrapper.querySelector('input')).toHaveFocus();

        userEvent.keyboard("{space}");
        userEvent.keyboard("{arrowdown}");
        userEvent.keyboard("{arrowdown}");
        userEvent.keyboard("{space}");

        expect(screen.getByText("Vanilla")).toBeInTheDocument();
        expect(screen.queryByText("chocolate")).not.toBeInTheDocument();
    });
});
