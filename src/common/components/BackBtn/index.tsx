import { FC } from "react";
import { useNavigate } from "react-router-dom";

const BackBtn: FC = () => {
  const navigate = useNavigate();

  const backBtnHandler = () => {
    navigate("/");
  };

  return (
    <div>
      <button
        type="button"
        aria-label="Back To Restaurant Select"
        onClick={backBtnHandler}
      >
        Back
      </button>
    </div>
  );
};

export default BackBtn;
