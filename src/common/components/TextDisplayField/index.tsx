import { useState, useMemo } from "react";
import { getIdV4 } from "../../utils/getUniqueIds";

import styles from "./index.module.css";

interface OwnProps {
  label: string;
  value: string;
}

const TextDisplayField: React.FC<OwnProps> = ({ label, value }) => {
  const valueFieldId = useMemo(() => getIdV4(), []);
  return (
    <div className={styles["field-wrapper"]} aria-label={`${label}, ${value}.`}>
      <label
        htmlFor={valueFieldId}
        aria-hidden="true"
        className={styles["field-label"]}
      >
        {label}
      </label>
      <div
        id={valueFieldId}
        aria-hidden="true"
        className={styles["field-value"]}
      >
        {value}
      </div>
    </div>
  );
};

export default TextDisplayField;
