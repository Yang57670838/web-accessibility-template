import { useContext } from "react";
import LiveAnnounceContext, {
  LiveAnnounceContextInterface,
} from "../contexts/LiveAnnounceContext";

export const useLiveAnnounceContext = () => {
  const liveAnnounceState = useContext<LiveAnnounceContextInterface | null>(
    LiveAnnounceContext
  );
  return liveAnnounceState;
};
