import React from "react";
import { Routes, Route } from "react-router-dom";
import RootContainer from "./screens/RootContainer";
import DominoLandPage from "./screens/Domino";
import KfcLandPage from "./screens/Kfc";

import styles from "./App.module.css";

const App = () => {
  if (process.env.NODE_ENV !== "production") {
    window.addEventListener("focusin", (event) => {
      console.log("current focusing on", document.activeElement);
    });
  }
  return (
    <div>
      <Routes>
        <Route index element={<RootContainer />} />
        <Route path="domino" element={<DominoLandPage />} />
        <Route path="kfc" element={<KfcLandPage />} />
      </Routes>
    </div>
  );
};

export default App;
