import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import "./index.css";
import App from "./App";
import { LiveAnnounceStore } from "./common/contexts/LiveAnnounceContext";
import reportWebVitals from "./reportWebVitals";

if (process.env.NODE_ENV !== "production") {
  /* eslint-disable global-require */
  const axe = require("@axe-core/react");
  axe(React, ReactDOM, 1000);
}

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <LiveAnnounceStore>
        <App />
      </LiveAnnounceStore>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
