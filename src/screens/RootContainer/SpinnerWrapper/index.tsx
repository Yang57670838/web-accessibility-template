import * as React from "react";
import Spinner from "../../../common/components/Spinner";

// TODO: show only when needed, also aria hide background when showing
const SpinnerWrapper = <T,>(WrappedComponent: React.ComponentType<T>) => {
  const HOC = (props: T) => {
    // TODO: logic here
    const show = false;
    return (
      <>
        {show && <Spinner />}
        <div aria-hidden={show ? "true" : "false"}>
          {/* eslint-disable-next-line */}
          <WrappedComponent {...props} />
        </div>
      </>
    );
  };
  return HOC;
};

export default SpinnerWrapper;
