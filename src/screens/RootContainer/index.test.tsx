import { screen, render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import RootContainer from './index';
import {BrowserRouter, MemoryRouter} from 'react-router-dom'

describe('<RootContainer />', () => {
    afterEach(() => {
        jest.resetAllMocks();
    })

    test('test first screen elements are focusable or not', () => {
        render(<RootContainer />, {wrapper: BrowserRouter})
        
        // header have auto focus
        const header = screen.getByText("Web Accessibility Template");
        expect(header).toBeInTheDocument();
        expect(header).toHaveFocus();
    
        // link must have focus, so can go to other pages directly
        const dominoLink = screen.getByText("Domino");
        expect(dominoLink).toBeInTheDocument();
        dominoLink.focus();
        expect(dominoLink).toHaveFocus();
    
        const kfcLink = screen.getByText("KFC");
        expect(kfcLink).toBeInTheDocument();
        kfcLink.focus();
        expect(kfcLink).toHaveFocus();
    });
});
