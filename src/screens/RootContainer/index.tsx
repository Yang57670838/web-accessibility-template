import * as React from "react";
import { Link } from "react-router-dom";
import { useLiveAnnounceContext } from "../../common/hooks/useLiveAnnounceContext";
import { getIdV4 } from "../../common/utils/getUniqueIds";
import PageHeading from "../../common/components/PageHeading";
import Contact from "../Contact";
import Footer from "./Footer";
import SpinnerWrapper from "./SpinnerWrapper";

import styles from "./index.module.css";

// screen to select different restaurants or services
const RootContainer: React.FC = () => {
  const liveAnnounceState = useLiveAnnounceContext();
  React.useEffect(() => {
    // test purpose, will not announce this instead of h1
    liveAnnounceState?.announceAssertive(
      "Welcome to Web Accessibility Template",
      getIdV4()
    );
  }, []);
  return (
    <div>
      {/* <SpinnerWrapper /> */}
      <div role="main">
        <PageHeading heading1="Web Accessibility Template" />
        <Link to="domino" className={styles["select-link"]}>
          Domino
        </Link>
        <Link to="kfc" className={styles["select-link"]}>
          KFC
        </Link>
      </div>
      {/* TODO: use talking machine or chatbox to switch focus control */}
      <Contact />
      <Footer />
    </div>
  );
};

export default SpinnerWrapper(RootContainer);
