import * as React from "react";

import styles from "./index.module.css";

const Footer: React.FC = () => {
  return (
    <div role="contentinfo">
      <div aria-hidden="true" className={styles["footer-wrapper"]}>
        logo
      </div>
    </div>
  );
};

export default Footer;
