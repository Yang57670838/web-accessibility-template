import * as React from "react";
import PageHeading from "../../common/components/PageHeading";
import BackBtn from "../../common/components/BackBtn";

const KfcLandPage: React.FC = () => {
  return (
    <div>
      <PageHeading heading1="KfcLandPage" />
      <BackBtn />
    </div>
  );
};

export default KfcLandPage;
