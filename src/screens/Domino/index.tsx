import * as React from "react";
import PageHeading from "../../common/components/PageHeading";
import CustomSelect from "../../common/components/CustomSelect";
import BackBtn from "../../common/components/BackBtn";

const DominoLandPage: React.FC = () => {
  return (
    <div>
      <PageHeading heading1="DominoLandPage" />
      <CustomSelect />
      <BackBtn />
    </div>
  );
};

export default DominoLandPage;
