import * as React from "react";

const Contact: React.FC = () => {
  // TODO
  const hasError = true;
  return (
    <div>
      <form>
        <label htmlFor="confirmCheckbox">
          Please check your contact information is correct{" "}
          <input type="checkbox" id="confirmCheckbox" aria-invalid={hasError} />
        </label>
      </form>
    </div>
  );
};

export default Contact;
